//
// Author:      Yanik Ammann (confused@ant.lgbt)
// Last-Change: 17.03.2020
//

const settingMaxEntryLength = 18;

const useTimeBasedGreetings = false;
const selectedGreeting = 0; // 0 for random
const useNicknames = true;
const username = "Yanik";
const selectedNicknameCollection = 1; // 0 for all
const selectedNickname = 0; // 0 for random

const greetings = [ "Hey", "Hi", "Hello", "G'Day" ];
const nicknames = [ [ "Cute", "Cutie", "Sweetie", "Sweetheart", "Darling", "Loverboy" ], [ "Offensive", "Idiot" ] ];

const titleColumns = [ "Reddit", "Tools", "Gitlab", "Weather" ];
const contentColumns = [ ['r/GayTeensVerified', 'r/LinuxMasterRace', 'r/UnixPorn'], ["test"], "ConfusedAnt", "Unteraegeri" ];

updateView();

function updateView() {
    updateGreeting();
    updateColumn();
}

function updateGreeting() {
    let greeting;
    if (useTimeBasedGreetings == true) {
        // use time based greetings
        var hour = (new Date).getHours();
        if (hour < 7 || hour > 21)
            greeting = "Good Night"
        else if (hour < 12)
            greeting = "Good Morning"
        else if (hour < 17)
            greeting = "Good Afternoon"
        else 
            greeting = "Good Evening"
    } else {
        // use normal greetings
        if (selectedGreeting == 0) {
            // random
            greeting = greetings[Math.floor(Math.random() * (greetings.length - 1)) + 1];
        } else {
            greeting = greetings[selectedGreeting - 1];
        }
    }
    let nickname = username;
    if (useNicknames == true) {
        var nicknamecollectionToUse;
        if (selectedNicknameCollection == 0) {
            // random
            nicknamecollectionToUse = nicknames[Math.floor(Math.random() * nicknames.length + 1)]
        } else {
            nicknamecollectionToUse = nicknames[selectedNicknameCollection - 1];
        }
        // get nickname from collection
        if (selectedNickname == 0) {
            nickname = nicknamecollectionToUse[Math.floor(Math.random() * (nicknamecollectionToUse.length - 1)) + 1]
        } else {
            nickname = nicknamecollectionToUse[selectedNickname];
        }
    }
    document.getElementsByTagName("h1")[0].innerHTML = greeting + ", " + nickname;
}

function updateColumn() {
    console.log(contentColumns);
    for (let i = 0; i < titleColumns.length; i++) {
        // check what is in this Column
        switch (titleColumns[i]) {
            case "Reddit":
                var output = [];
                for (let j = 0; j < contentColumns[i].length; j++) {
                    array = contentColumns[i];
                    output.push(array[j]);
                    output.push("https://reddit.com/" + array[i].toLowerCase());
                }
                fillLinks(i, output);
                break;
            case "Gitlab":
                getGitlab(i, contentColumns[i])
                break;
            case "Weather":
                getWeather(i, contentColumns[i])
                break;
            default:
                fillLinks(i, contentColumns[i]);
                break;
        }
    }
}

function getGitlab(column, username) {
    async function getData() {
        let response = await fetch("https://gitlab.com/api/v4/users/" + username + "/projects?visiblity=pulbic&order_by=updated_at&archived=no");
        let data = await response.json();
        return data;
    }
    getData().then(data => {
        var output = [];
        console.log(data);
        for (let i = 0; i < data.length; i++) {
            output.push(data[i].name);
            output.push("https://gitlab.com/" + username + "/" + data[i].path);
        }
        fillLinks(column, output);
    })
}

function getWeather(column, location) {
    async function getData() {
        let response = await fetch("https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=a789b4a070760ac4e3f8c44a3115c98d");
        let data = await response.json();
        return data;
    }
    getData().then(data => {
        var output = [];
        for (let i = 0; i < data.length; i++)
            output.push(data[i].name);
        fillLinks(column, output);
    })
}

function shoten(input) {
    if (input.length > settingMaxEntryLength)
       return input.substring(0, settingMaxEntryLength - 3) + '...';
    else
       return input;
 };

function fillLinks(columnID, array) {
    for (let index = 0; index < array.length; index += 2) {
        var node = document.createElement("a");
        node.appendChild(document.createTextNode(shoten(array[index])));
        node.appendChild(document.createElement("br"));
        node.href = array[index + 1];
        document.getElementById("column" + (columnID + 1)).appendChild(node)
    }
}